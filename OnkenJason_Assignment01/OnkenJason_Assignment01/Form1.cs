﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

/*
 * Jason Onken
 * Project and Portfolio III: Mobile Development - Online
 * 04/2017
 * Code Assignment 01
 */

namespace OnkenJason_Assignment01
{
    public partial class Form1 : Form
    {
        //Initialize and build the form
        public Form1()
        {
            InitializeComponent();
        }

        //Exit event handler
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Save event handler
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Create a new save file dialog window
            SaveFileDialog save = new SaveFileDialog();
            //Set the title of the dialog box
            save.Title = "Save File";
            //Set the default extension to a text file
            save.DefaultExt = "txt";

            //Run once the user clicks ok
            if (save.ShowDialog() == DialogResult.OK)
            {
                //Create a new stream writer to write the data to the file
                StreamWriter sw = new StreamWriter(File.Create(save.FileName));
                //Write the need header
                sw.WriteLine("Need");
                //Write out each item in the need list
                foreach (var item in listBoxNeed.Items)
                {
                    sw.WriteLine(item.ToString());
                }
                //Write the have header
                sw.WriteLine("\r\nHave");
                //Write out each item in the have list
                foreach (var item in listBoxHave.Items)
                {
                    sw.WriteLine(item.ToString());
                }
                //Close the file
                sw.Dispose();
            }
        }

        //Add event handler
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            //Make sure the text box is not empty or blank spaces
            if (!string.IsNullOrWhiteSpace(textBoxItem.Text))
            {
                //Check if either list already contains the item
                if (!listBoxNeed.Items.Contains(textBoxItem.Text.ToLower()) && !listBoxHave.Items.Contains(textBoxItem.Text.ToLower()))
                {
                    //Add the item
                    listBoxNeed.Items.Add(textBoxItem.Text.ToLower());
                }
                //Show the error if the item is in the list
                else MessageBox.Show(textBoxItem.Text + " is already in the list!");
                //Set the text box value to default
                textBoxItem.Text = "";
            }
            //Error message if the text box is blank
            else MessageBox.Show("Please input the item to add into the text box first!");

        }

        //Move to have event handler
        private void buttonHave_Click(object sender, EventArgs e)
        {
            //Make sure an item is selected
            if (listBoxNeed.SelectedIndices.Count == 1)
            {
                //Add the item to the need list
                listBoxHave.Items.Add(listBoxNeed.SelectedItem);
                //Remove the item from the have list
                listBoxNeed.Items.Remove(listBoxNeed.SelectedItem);
            }
            //Error message if no item is selected
            else MessageBox.Show("No item from the Need list selected to move!");
        }

        //Move to need event handler
        private void buttonNeed_Click(object sender, EventArgs e)
        {
            //Make sure an item is selected
            if (listBoxHave.SelectedIndices.Count == 1)
            {
                //Add the item to the have list
                listBoxNeed.Items.Add(listBoxHave.SelectedItem);
                //Remove the item from the need list
                listBoxHave.Items.Remove(listBoxHave.SelectedItem);
            }
            //Error message if no item is selected
            else MessageBox.Show("No item from the Have list selected to move!");
        }

        //Remove event handler
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            //Check if the need list has a selected item
            if (listBoxNeed.SelectedIndices.Count == 1)
            {
                //Remove that item from the list
                listBoxNeed.Items.Remove(listBoxNeed.SelectedItem);
            }
            //Check if the have list has a selected item
            else if (listBoxHave.SelectedIndices.Count == 1)
            {
                //Remove that item from the list
                listBoxHave.Items.Remove(listBoxHave.SelectedItem);
            }
            //Error message if no item is selected
            else MessageBox.Show("Please select an item to remove!");
        }

        //Need selected index event handler
        private void listBoxNeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            //If an item is selected in the need list, unselect any item from the have list
            if (listBoxNeed.SelectedIndex == 0)
            {
                listBoxHave.SelectedItem = null;
            }
        }

        //Have selected index event handler
        private void listBoxHave_SelectedIndexChanged(object sender, EventArgs e)
        {
            //If an item is selected in the have list, unselect any item from the need list
            if (listBoxHave.SelectedIndex == 0)
            {
                listBoxNeed.SelectedItem = null;
            }
        }
    }
}
