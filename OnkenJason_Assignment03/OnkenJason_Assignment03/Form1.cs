﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * Jason Onken
 * Project and Portfolio III - Online
 * 04/2017
 * Assignment 03
 */

namespace OnkenJason_Assignment03
{
    public partial class Form1 : Form
    {
        //Bool to check the turn
        //If true then player X turn, if false player O turn
        bool turn = true;
        //Variable to keep track of numbers of turns
        int countTurn = 0;
        //Variable to keep track of X and O win count
        int xWinCount = 0;
        int oWinCount = 0;
        //Variable to keep track of draws
        int drawCount = 0;
        

        //Build and load the form
        public Form1()
        {
            InitializeComponent();
        }

        //Exit event handler
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //New game event handler
        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            //Create a list to hold all the buttons
            List<Button> buttonList = new List<Button> { buttonA1, buttonA2, buttonA3, buttonB1, buttonB2, buttonB3, buttonC1, buttonC2, buttonC3 };
            //Set the buttons to enabled and remove the letter
            foreach (var b in buttonList)
            {
                b.Text = "";
                b.Enabled = true;
            }
            //Set the turn to player X
            turn = true;
            //Set the turn count back to 0
            countTurn = 0;

        }

        //Event handler for a button click
        //Each button will be passed as the parameter
        private void button_Click(object sender, EventArgs e)
        {
            //Create a button and cast the sender object to it
            Button b = sender as Button;
            //Check to see who's turn it is
            if (turn == true)
            {
                //Write an X if it is player X's turn
                b.Text = "X";
                //Disable the button until new game
                b.Enabled = false;
                //Change the bool to false for player O's turn
                turn = false;
            }
            //Write an O if it is player O's turn
            else
            {
                b.Text = "O";
                //Disable the button until new game
                b.Enabled = false;
                //Change the bool to true for player X's turn
                turn = true;
            }
            //Add one to the turn count
            countTurn++;
            //Run the check for winner method
            CheckForWinner(); 
        }

        //Method to check for a winner
        private void CheckForWinner()
        {
            //Row checks
            //Check if the first row all match
            if (buttonA1.Text == buttonA2.Text && buttonA2.Text == buttonA3.Text && buttonA1.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonA1.Text + " wins!");
                DisableButtons();
                WinCount();
            }
            //Check if the second row all match
            else if (buttonB1.Text == buttonB2.Text && buttonB2.Text == buttonB3.Text && buttonB1.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonB1.Text + " wins!");
                DisableButtons();
                WinCount();
            }
            //Check if the third row all match
            else if (buttonC1.Text == buttonC2.Text && buttonC2.Text == buttonC3.Text && buttonC1.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonC1.Text + " wins!");
                DisableButtons();
                WinCount();
            }

            //Column checks
            //Check if the first column all match
            else if (buttonA1.Text == buttonB1.Text && buttonB1.Text == buttonC1.Text && buttonA1.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonA1.Text + " wins!");
                DisableButtons();
                WinCount();
            }
            //Check if the second column all match
            else if (buttonA2.Text == buttonB2.Text && buttonB2.Text == buttonC2.Text && buttonA2.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonA2.Text + " wins!");
                DisableButtons();
                WinCount();
            }
            //Check if the third column all match
            else if (buttonA3.Text == buttonB3.Text && buttonB3.Text == buttonC3.Text && buttonA3.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonA3.Text + " wins!");
                DisableButtons();
                WinCount();
            }

            //Diagonal checks
            //Check if the diagonal all match
            else if (buttonA1.Text == buttonB2.Text && buttonB2.Text == buttonC3.Text && buttonA1.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonA1.Text + " wins!");
                DisableButtons();
                WinCount();
            }
            //Check if the other diagonal all match
            else if (buttonA3.Text == buttonB2.Text && buttonB2.Text == buttonC1.Text && buttonA3.Enabled == false)
            {
                //Show who the winner is and disable the other buttons
                MessageBox.Show("Player " + buttonA3.Text + " wins!");
                DisableButtons();
                WinCount();
            }

            //If the count reaches 9 there was a draw
            else
            {
                if (countTurn == 9)
                {
                    //Show the message for a draw
                    MessageBox.Show("It was a draw!");
                    drawCount++;
                    labelDraws.Text = "Draws: " + drawCount.ToString();
                }
            }
        }

        //Create a method to increase the counts for wins or draws
        private void WinCount()
        {
            //If turn equals true player O was the last to go
            if (turn == true)
            {
                //Increase the O win count
                oWinCount++;
                //Change the label to show the number of wins
                labelOWins.Text = "O Wins: " + oWinCount.ToString();
            }
            //If turn equals false player X was the last to go
            else
            {
                //Increase the X win count
                xWinCount++;
                //Change the label to show the number of wins
                labelXWins.Text = "X Wins: " + xWinCount.ToString();
            }
        }

        //Create a method to disable all the buttons
        private void DisableButtons()
        {
            //Create a list to hold all the buttons
            List<Button> buttonList = new List<Button> { buttonA1, buttonA2, buttonA3, buttonB1, buttonB2, buttonB3, buttonC1, buttonC2, buttonC3 };
            //Loop through each button
            foreach (var b in buttonList)
            {
                //Disable the button if it is still enabled
                if (b.Enabled == true)
                {
                    b.Enabled = false;
                }
            }
        }
    }
}
