﻿namespace OnkenJason_Assignment03
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonA1 = new System.Windows.Forms.Button();
            this.buttonA2 = new System.Windows.Forms.Button();
            this.buttonA3 = new System.Windows.Forms.Button();
            this.buttonB1 = new System.Windows.Forms.Button();
            this.buttonB2 = new System.Windows.Forms.Button();
            this.buttonB3 = new System.Windows.Forms.Button();
            this.buttonC1 = new System.Windows.Forms.Button();
            this.buttonC2 = new System.Windows.Forms.Button();
            this.buttonC3 = new System.Windows.Forms.Button();
            this.labelXWins = new System.Windows.Forms.Label();
            this.labelOWins = new System.Windows.Forms.Label();
            this.buttonNewGame = new System.Windows.Forms.Button();
            this.labelDraws = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(465, 37);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(61, 33);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 34);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // buttonA1
            // 
            this.buttonA1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonA1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonA1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonA1.Location = new System.Drawing.Point(102, 121);
            this.buttonA1.Name = "buttonA1";
            this.buttonA1.Size = new System.Drawing.Size(75, 75);
            this.buttonA1.TabIndex = 1;
            this.buttonA1.UseVisualStyleBackColor = false;
            this.buttonA1.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonA2
            // 
            this.buttonA2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonA2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonA2.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonA2.Location = new System.Drawing.Point(195, 121);
            this.buttonA2.Name = "buttonA2";
            this.buttonA2.Size = new System.Drawing.Size(75, 75);
            this.buttonA2.TabIndex = 2;
            this.buttonA2.UseVisualStyleBackColor = false;
            this.buttonA2.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonA3
            // 
            this.buttonA3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonA3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonA3.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonA3.Location = new System.Drawing.Point(288, 121);
            this.buttonA3.Name = "buttonA3";
            this.buttonA3.Size = new System.Drawing.Size(75, 75);
            this.buttonA3.TabIndex = 3;
            this.buttonA3.UseVisualStyleBackColor = false;
            this.buttonA3.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonB1
            // 
            this.buttonB1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonB1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonB1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonB1.Location = new System.Drawing.Point(102, 215);
            this.buttonB1.Name = "buttonB1";
            this.buttonB1.Size = new System.Drawing.Size(75, 75);
            this.buttonB1.TabIndex = 6;
            this.buttonB1.UseVisualStyleBackColor = false;
            this.buttonB1.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonB2
            // 
            this.buttonB2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonB2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonB2.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonB2.Location = new System.Drawing.Point(195, 215);
            this.buttonB2.Name = "buttonB2";
            this.buttonB2.Size = new System.Drawing.Size(75, 75);
            this.buttonB2.TabIndex = 5;
            this.buttonB2.UseVisualStyleBackColor = false;
            this.buttonB2.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonB3
            // 
            this.buttonB3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonB3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonB3.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonB3.Location = new System.Drawing.Point(288, 215);
            this.buttonB3.Name = "buttonB3";
            this.buttonB3.Size = new System.Drawing.Size(75, 75);
            this.buttonB3.TabIndex = 4;
            this.buttonB3.UseVisualStyleBackColor = false;
            this.buttonB3.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonC1
            // 
            this.buttonC1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonC1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonC1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonC1.Location = new System.Drawing.Point(102, 307);
            this.buttonC1.Name = "buttonC1";
            this.buttonC1.Size = new System.Drawing.Size(75, 75);
            this.buttonC1.TabIndex = 9;
            this.buttonC1.UseVisualStyleBackColor = false;
            this.buttonC1.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonC2
            // 
            this.buttonC2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonC2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonC2.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonC2.Location = new System.Drawing.Point(195, 307);
            this.buttonC2.Name = "buttonC2";
            this.buttonC2.Size = new System.Drawing.Size(75, 75);
            this.buttonC2.TabIndex = 8;
            this.buttonC2.UseVisualStyleBackColor = false;
            this.buttonC2.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonC3
            // 
            this.buttonC3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonC3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonC3.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buttonC3.Location = new System.Drawing.Point(288, 307);
            this.buttonC3.Name = "buttonC3";
            this.buttonC3.Size = new System.Drawing.Size(75, 75);
            this.buttonC3.TabIndex = 7;
            this.buttonC3.UseVisualStyleBackColor = false;
            this.buttonC3.Click += new System.EventHandler(this.button_Click);
            // 
            // labelXWins
            // 
            this.labelXWins.AutoSize = true;
            this.labelXWins.Font = new System.Drawing.Font("Comic Sans MS", 9.857143F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelXWins.Location = new System.Drawing.Point(13, 58);
            this.labelXWins.Name = "labelXWins";
            this.labelXWins.Size = new System.Drawing.Size(134, 35);
            this.labelXWins.TabIndex = 10;
            this.labelXWins.Text = "X Wins: 0";
            // 
            // labelOWins
            // 
            this.labelOWins.AutoSize = true;
            this.labelOWins.Font = new System.Drawing.Font("Comic Sans MS", 9.857143F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOWins.Location = new System.Drawing.Point(171, 58);
            this.labelOWins.Name = "labelOWins";
            this.labelOWins.Size = new System.Drawing.Size(136, 35);
            this.labelOWins.TabIndex = 11;
            this.labelOWins.Text = "O Wins: 0";
            // 
            // buttonNewGame
            // 
            this.buttonNewGame.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewGame.Location = new System.Drawing.Point(140, 417);
            this.buttonNewGame.Name = "buttonNewGame";
            this.buttonNewGame.Size = new System.Drawing.Size(190, 65);
            this.buttonNewGame.TabIndex = 12;
            this.buttonNewGame.Text = "New Game";
            this.buttonNewGame.UseVisualStyleBackColor = true;
            this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // labelDraws
            // 
            this.labelDraws.AutoSize = true;
            this.labelDraws.Font = new System.Drawing.Font("Comic Sans MS", 9.857143F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDraws.Location = new System.Drawing.Point(334, 58);
            this.labelDraws.Name = "labelDraws";
            this.labelDraws.Size = new System.Drawing.Size(120, 35);
            this.labelDraws.TabIndex = 13;
            this.labelDraws.Text = "Draws: 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(465, 506);
            this.Controls.Add(this.labelDraws);
            this.Controls.Add(this.buttonNewGame);
            this.Controls.Add(this.labelOWins);
            this.Controls.Add(this.labelXWins);
            this.Controls.Add(this.buttonC1);
            this.Controls.Add(this.buttonC2);
            this.Controls.Add(this.buttonC3);
            this.Controls.Add(this.buttonB1);
            this.Controls.Add(this.buttonB2);
            this.Controls.Add(this.buttonB3);
            this.Controls.Add(this.buttonA3);
            this.Controls.Add(this.buttonA2);
            this.Controls.Add(this.buttonA1);
            this.Controls.Add(this.menuStrip);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Tic Tac Toe";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button buttonA2;
        private System.Windows.Forms.Button buttonA3;
        private System.Windows.Forms.Button buttonB1;
        private System.Windows.Forms.Button buttonB2;
        private System.Windows.Forms.Button buttonB3;
        private System.Windows.Forms.Button buttonC1;
        private System.Windows.Forms.Button buttonC2;
        private System.Windows.Forms.Button buttonC3;
        private System.Windows.Forms.Label labelXWins;
        private System.Windows.Forms.Label labelOWins;
        private System.Windows.Forms.Button buttonNewGame;
        private System.Windows.Forms.Label labelDraws;
        protected System.Windows.Forms.Button buttonA1;
    }
}

