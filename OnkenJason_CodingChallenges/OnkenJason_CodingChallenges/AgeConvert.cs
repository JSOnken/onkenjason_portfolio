﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnkenJason_CodingChallenges
{
    class AgeConvert
    {

        //Method to convert the user's age
        public void ageConvert()
        {
            //Tell the user what we are doing and ask for their name and age
            Console.WriteLine("Hello, please enter your name:");
            //Catch and store the response
            string userName = Console.ReadLine();
            //Make sure they didn't leave it blank
            while (string.IsNullOrWhiteSpace(userName))
            {
                //Tell the user the what happened and have them enter their name again
                Console.WriteLine("Please do not leave this blank.\r\nEnter your name:");
                //Re-catch and store the response
                userName = Console.ReadLine();
            }
            //Ask the user for the year
            Console.WriteLine("Great " + userName + ", we are going to convert your age.\r\nPlease enter the year you were born:");
            //Catch and store the response
            string yearString = Console.ReadLine();
            //Create an int to save the year
            int yearInt;
            //Make sure they entered a number
            while (!int.TryParse(yearString, out yearInt))
            {
                //Tell the user the error if they did not enter a number
                Console.WriteLine("You did not enter a number.\r\nPlease enter the year:");
                //Re-catch and store the response
                yearString = Console.ReadLine();
                //Try to convert to a number again
                int.TryParse(yearString, out yearInt);
            }
            //Ask the user for the month
            Console.WriteLine("Now please enter the month in numeric format (1-12):");
            //Catch and store the response
            string monthString = Console.ReadLine();
            //Create an int to store the month
            int monthInt;
            //Make sure they entered a number
            while (!int.TryParse(monthString, out monthInt))
            {
                //Tell the user the error if they did not enter a number
                Console.WriteLine("You did not enter a number.\r\nPlease enter the month in numeric format:");
                //Re-catch and store the response
                monthString = Console.ReadLine();
                //Try to convert to a number again
                int.TryParse(monthString, out monthInt);
                //Make sure it is between 1 and 12
                if (monthInt < 1 || monthInt > 12)
                {
                    //Tell the user the error
                    Console.WriteLine("You did not enter a number between 1 and 12.\r\nPlease enter the month:");
                    //Re-catch and store the response
                    monthString = Console.ReadLine();
                }
            }
            //Ask the user for the day
            Console.WriteLine("Now please enter the day:");
            //Catch and store the response
            string dayString = Console.ReadLine();
            //Create an int to store the day
            int dayInt;
            //Make sure they entered a number
            while (!int.TryParse(dayString, out dayInt))
            {
                //Tell the user the error if they did not enter a number
                Console.WriteLine("You did not enter a number.\r\nPlease enter the day:");
                //Re-catch and store the response
                dayString = Console.ReadLine();
                //Try to convert to a number again
                int.TryParse(dayString, out dayInt);
                //Make sure it's between 1 and 31
                if (dayInt < 1 || dayInt > 31)
                {
                    //Tell the user the error
                    Console.WriteLine("You did not enter a number between 1 and 31.\r\nPlease enter the day:");
                    //Re-catch and store the response
                    dayString = Console.ReadLine();
                }
            }
            //Create an array to hold the birthday
            DateTime birthdate = new DateTime(yearInt, monthInt, dayInt);
            //Calculate the age
            TimeSpan myAge = DateTime.Now.Subtract(birthdate);
            //Write out the number of days the user has been alive
            Console.WriteLine("The number of days you've been alive is " + Math.Round(myAge.TotalDays) + "\r\nThe number of hours you've been alive is " + Math.Round(myAge.TotalHours) + "\r\nThe number of minutes you've been alive is " + Math.Round(myAge.TotalMinutes) + "\r\nThe number of seconds you've been alive is " + Math.Round(myAge.TotalSeconds));
        }
    }
}
