﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnkenJason_CodingChallenges
{
    class Program
    {
        static void Main(string[] args)
        {

            //Create a menu for the user to pick which method they would like to run
            Console.WriteLine("Hello, please pick the method you would like to run by selecting the corresponding number.\r\n1 - Swap Name\r\n2 - Backwards\r\n3 - Age Convert\r\n4 - Temperature Converter");

            //Catch and store the user's response
            string numSelectedString = Console.ReadLine();

            //Check the user's responses
            //Go to SwapName class if they selected 1
            if (numSelectedString == "1")
            {
                //Call on the SwapName class
                SwapName newName = new SwapName();
                //Run the swapName method
                newName.swapName();
            }
            //Go to Backwards class if they selected 2
            else if (numSelectedString == "2")
            {
                //Call on the Backwards class
                Backwards newSentence = new Backwards();
                //Run the backwards method
                newSentence.backwards();
            }
            //Go to AgeConvert class if they selected 3
            else if (numSelectedString == "3")
            {
                //Call on the AgeConvert class
                AgeConvert newAge = new AgeConvert();
                //Run the ageConvert method
                newAge.ageConvert();
            }
            //Go to TempConvert if they selected 4
            else if (numSelectedString == "4")
            {
                //Call on the TempConvert class
                TempConvert newTemp = new TempConvert();
                //Run the tempConvert method
                newTemp.tempConvert();
            }
            //Create a condition if the response is invalid
            else
            {
                //Tell the user the error
                Console.WriteLine("You did not enter a valid response. Please pick your program to run.\r\n1 - Swap Name\r\n2 - Backwards\r\n3 - Age Convert\r\n4 - Temperature Converter");
                numSelectedString = Console.ReadLine();
            }
        }
    }
}
