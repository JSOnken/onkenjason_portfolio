﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnkenJason_CodingChallenges
{
    class SwapName
    {

        //Method that will be ran for name swap
        public void swapName()
        {
            //Tell the user what we are doing and ask for their first name
            Console.WriteLine("We are going to swap things around!\r\nPlease enter your first name:");
            //Catch and store the response
            string firstName = Console.ReadLine();
            //Make sure it wasn't left blank
            while (string.IsNullOrWhiteSpace(firstName))
            {
                //Tell the user the error and ask for their name again
                Console.WriteLine("Please do not leave this blank.\r\nEnter your first name:");
                //Recatch and store the response
                firstName = Console.ReadLine();
            }
            //Ask for the last name
            Console.WriteLine("Great job!\r\nNow enter your last name:");
            //Catch and store the response 
            string lastName = Console.ReadLine();
            //Make sure it wasn't left blank
            while (string.IsNullOrWhiteSpace(lastName))
            {
                //Tell the user what happened and ask for their name again
                Console.WriteLine("Please do not leave this blank.\r\nEnter your last name:");
                //Recatch and store the response
                lastName = Console.ReadLine();
            }
            //Write out the user's name
            Console.WriteLine("Your name is " + firstName + " " + lastName);
            //Write out the user's name backwards
            Console.WriteLine("Your name backwards is " + lastName + " " + firstName);
        }

    }
}
