﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnkenJason_CodingChallenges
{
    class TempConvert
    {

        //Method to convert the temperature
        public void tempConvert()
        {
            //Tell the user what we are doing and ask for a Fahrenheit temp
            Console.WriteLine("We are going to convert some temperatures.\r\nPlease start by entering a temperature in Fahrenheit to convert:");
            //Catch and store the response
            string tempStringFahrenheit = Console.ReadLine();
            //Create an int to hold the number
            int tempNumberFahrenheit;
            //Convert the string to a number
            while (!int.TryParse(tempStringFahrenheit, out tempNumberFahrenheit))
            {
                //Tell the user the error if they did not enter a number
                Console.WriteLine("You did not enter a number.\r\nPlease enter a number:");
                //Re-catch and store the user's response
                tempStringFahrenheit = Console.ReadLine();
                //Try to convert it to a number
                int.TryParse(tempStringFahrenheit, out tempNumberFahrenheit);
            }
            //Convert the temperature to Celcius
            int tempConvertCelcius = ((tempNumberFahrenheit - 32) * 5) / 9;
            //Write out the temperature and conversion to the user
            Console.WriteLine("The temperature you entered is " + tempStringFahrenheit + "F.\r\nIt converts to " + tempConvertCelcius + "C.");

            //Ask the user to input a temperature in Celcius
            Console.WriteLine("You will now enter a temperature in Celcius to convert to Fahrenheit:");
            //Catch and store the response
            string tempStringCelcius = Console.ReadLine();
            //Create an int to hold the number
            int tempNumberCelcius;
            //Convert the string to a number
            while (!int.TryParse(tempStringCelcius, out tempNumberCelcius))
            {
                //Tell the user the error if they did not enter a number
                Console.WriteLine("You did not enter a number.\r\nPlease enter a number:");
                //Re-catch and store the user's response
                tempStringCelcius = Console.ReadLine();
                //Try to convert it to a number
                int.TryParse(tempStringCelcius, out tempNumberCelcius);
            }
            //Convert the temperature to Fahrenheit
            double tempConvertFahrenheit = (tempNumberCelcius * 1.8) + 32;
            //Write out the temperature and conversion to the user
            Console.WriteLine("The temperature you entered is " + tempStringCelcius + "C.\r\nIt converts to " + tempConvertFahrenheit + "F.");
        }
    }
}
