﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnkenJason_CodingChallenges
{
    class Backwards
    {

        //Method that will run to reverse a sentence
        public void backwards()
        {
            //Tell the user what we are doing and ask for a sentence
            Console.WriteLine("We are going to reverse a sentence.\r\nPlease enter a sentence with at least 6 words.");
            //Catch and store the user's response
            string newSentenceString = Console.ReadLine();
            //Make sure they didn't leave it blank
            while (string.IsNullOrWhiteSpace(newSentenceString))
            {
                //Tell the user the error and ask for the sentence again
                Console.WriteLine("Please do not leave this blank.\r\nEnter the sentence:");
                //Recatch and store the user's response
                newSentenceString = Console.ReadLine();
            }
            //Create an array to store the words
            string[] wordArray = newSentenceString.Split(' ');
            //Reverse the array
            Array.Reverse(wordArray);
            //Print the sentence the user entered
            Console.WriteLine("The sentence you entered is:\r\n" + newSentenceString);
            //Join the array in a string and print it
            Console.WriteLine("The sentence in reverse is:\r\n" + string.Join(" ", wordArray));

        }
    }
}