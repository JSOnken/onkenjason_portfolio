﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

/*
 * Jason Onken
 * Project and Portfolio III: Mobile Development - Online
 * 04/2017
 * Assignment 02
 */

namespace OnkenJason_Assignment02
{
    public partial class Form1 : Form
    {
        //Custom event handler for modifying a contact
        private EventHandler<ModifyContactEventArgs> ModifyContact;

        //Class for the custom event
        class ModifyContactEventArgs : EventArgs
        {
            //Create a new listview item
            ListViewItem contactToModify;
            //Properties for getting and setting the values
            public ListViewItem ContactToModify
            {
                get { return contactToModify; }
                set { contactToModify = value; }
            }
            //Set the contact object to the selected listview item
            public ModifyContactEventArgs(ListViewItem lvi)
            {
                contactToModify = lvi;
            }
        }

        //Build and intialize the form
        public Form1()
        {
            //Load the custom event handler
            ModifyContact += HandleModifiedContact;
            InitializeComponent();
        }

        //Small icon event handler
        private void smallIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Check the small icon menu item
            smallIconsToolStripMenuItem.Checked = true;
            //Uncheck the large icon menu item
            largeIconsToolStripMenuItem.Checked = false;
            //Set the icon to the small imagelist
            listViewContacts.View = View.SmallIcon;
        }

        //Large icon event handler
        private void largeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Check the large icon menu item
            largeIconsToolStripMenuItem.Checked = true;
            //Uncheck the small icon menu item
            smallIconsToolStripMenuItem.Checked = false;
            //Set the icon to the large imagelist
            listViewContacts.View = View.LargeIcon;
        }

        //Add button event handler
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            //Create a new listview item
            ListViewItem lvi = new ListViewItem();
            //Create a new contact
            Contact c = new Contact();
            //Run the method to check format
            //If true save the values
            if (CheckFormat() == true)
            {
                //Save the values to the character
                c.FirstName = textBoxFirstName.Text;
                c.LastName = textBoxLastName.Text;
                c.PhoneNumber = textBoxPhone.Text;
                c.Email = textBoxEmail.Text;
                //Populate the list view with the contact data
                lvi.Text = c.ToString();
                lvi.ImageIndex = 0;
                lvi.Tag = c;
                //Add the contact to the list view
                listViewContacts.Items.Add(lvi);
                //Set the input fields to default
                SetDefualt();
            }
            //If not, show the error
            else MessageBox.Show("Please check your inputs.");
        }

        //Exit event handler
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Delete event handler
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            //Make sure an item is selected
            if (listViewContacts.SelectedIndices.Count == 1)
            {
                //Remove the selected item
                listViewContacts.Items.Remove(listViewContacts.SelectedItems[0]);
                //Set the input fields to default
                SetDefualt();
                //Hide the edit and delete buttons
                buttonEdit.Visible = false;
                buttonDelete.Visible = false;
            }
            //Error message if no item is selected
            else MessageBox.Show("Please select a contact to remove!");
        }

        private void listViewContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Make sure an item is selected
            if (listViewContacts.SelectedItems.Count > 0)
            {
                //Create a new contact to hold the data
                Contact selectedContact = listViewContacts.SelectedItems[0].Tag as Contact;
                //Load the data into the input controls
                textBoxFirstName.Text = selectedContact.FirstName;
                textBoxLastName.Text = selectedContact.LastName;
                textBoxPhone.Text = selectedContact.PhoneNumber;
                textBoxEmail.Text = selectedContact.Email;
            }
            //If non is selected set it to a new contact
            else new Contact();
            //Make the edit and delete buttons visible
            buttonEdit.Visible = true;
            buttonDelete.Visible = true;
        }

        //Edit button event handler
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            //Make sure an item is selected
            if (ModifyContact != null && listViewContacts.SelectedItems.Count > 0)
            {
                //Call the custom event handler
                ModifyContact(this, new ModifyContactEventArgs(listViewContacts.SelectedItems[0]));
            }
        }

        //Custom event handler for modifying a contact
        private void HandleModifiedContact(object sender, ModifyContactEventArgs e)
        {
            //Create a new contact from the selected item
            Contact c = e.ContactToModify.Tag as Contact;
            //If true save the values
            if (CheckFormat() == true)
            {
                //Set the values to the contact
                c.FirstName = textBoxFirstName.Text;
                c.LastName = textBoxLastName.Text;
                c.PhoneNumber = textBoxPhone.Text;
                c.Email = textBoxEmail.Text;
                //Populate the list view with the contact data
                e.ContactToModify.Text = c.ToString();
                e.ContactToModify.ImageIndex = 0;
                //Set the input fields to default
                SetDefualt();
                //Hide the edit and delete buttons
                buttonEdit.Visible = false;
                buttonDelete.Visible = false;
            }
            //If not, show the error
            else MessageBox.Show("Please check your inputs.");
        }

        //Method for setting the input fields to defualt
        private void SetDefualt()
        {
            textBoxFirstName.Text = "";
            textBoxLastName.Text = "";
            textBoxPhone.Text = "";
            textBoxEmail.Text = "";
        }

        //Method for checking name strings
        private bool CheckFormat()
        {
            //Make sure the first name is the correct format
            foreach (char c in textBoxFirstName.Text)
            {
                if (!char.IsLetter(c))
                {
                    return false;
                }
            }
            //Make sure the last name is the correct format
            foreach (char c in textBoxLastName.Text)
            {
                if (!char.IsLetter(c))
                {
                    return false;
                }
            }
            //Regular expression for phone number format
            //Will accept phone number entered with any, or no delimiter type
            Regex phoneNumber = new Regex(@"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$");
            //Make sure the phone number is the correct format
            if (!phoneNumber.IsMatch(textBoxPhone.Text))
            {
                return false;
            }
            //Regular expression for email format
            Regex emailFormat = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            //Make sure the email is in the correct format
            if (!emailFormat.IsMatch(textBoxEmail.Text))
            {
                return false;
            }
            return true;
        }
    }
}
