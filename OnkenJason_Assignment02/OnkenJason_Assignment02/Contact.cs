﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnkenJason_Assignment02
{
    class Contact
    {
        //Variables for the contact
        string firstName;
        string lastName;
        string phoneNumber;
        string email;

        //Properties to set the values
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        //Override the toString method
        public override string ToString()
        {
            return firstName + " " + lastName + " " + phoneNumber;
        }
    }
}
